import java.util.HashMap;
public class inventory <T>{
    // item contains name and price while integer will hold number of this item
    HashMap<T, Integer> inventoryItems=  new HashMap<T, Integer>();

    public void addItem ( T t, int quantity){
        inventoryItems.put(t , quantity);
    }
    public int getItem( T t ) {
        Integer value = inventoryItems.get(t);
        return value  ;
    }

    public boolean hasItem(T t ){
        return getItem(t) > 0;
    }

    public void clear(){
        inventoryItems.clear();
    }

    public void put(T item, int quantity) {
        inventoryItems.put(item, quantity);
    }

    public void add(T item){
        int count = inventoryItems.get(item);
        inventoryItems.put(item, count+1);
    }

    public void sell (T t ){
        if ((hasItem(t)) ==true){
            int count = inventoryItems.get(t);
            inventoryItems.put(t, count - 1);
        }
    }
}
