
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class vendingMachine {

    private inventory<coin> cashInventory = new inventory<coin>();
    private inventory<item> itemInventory = new inventory<item>();
    private long totalSales;
    private item currentItem;
    private long currentBalance;

    public vendingMachine(){
        initialize();
    }

    private void initialize(){
        //initialize machine with 5 coins of each denomination
        //and 5 products of each Item
        for(coin c : coin.values()){
            cashInventory.put(c, 5);
        }

        for(item i : item.values()){
            itemInventory.put(i, 5);
        }

    }

    public long selectItemAndGetPrice(item item) {
        if(itemInventory.hasItem(item)){
            currentItem = item;
            System.out.println("An item was purchased " + item);
            return currentItem.getPrice();
        }
        throw new SoldOutException("Sold Out, Please buy another item");
    }

    public void insertCoin(coin coin) {
        currentBalance = currentBalance + coin.getValue();
        System.out.println("A coin was inserted " + coin);
        cashInventory.add(coin);
    }


    public Bucket<item, List<coin>> collectItemAndChange() {
        item item = collectItem();
        totalSales = totalSales + currentItem.getPrice();

        List<coin> change = collectChange();

        return new Bucket<item, List<coin>>(item, change);
    }

    private item collectItem() throws NotSufficientPaidException,
            NotSufficientPaidException{
        if(isFullPaid()){
            if(hasSufficientChange()){
                itemInventory.sell(currentItem);
                return currentItem;
            }
            throw new NoChangeAvailableException("Not Sufficient change in Inventory");

        }
        long remainingBalance = currentItem.getPrice() - currentBalance;
        throw new NotSufficientPaidException("Price not full paid, remaining : ",
                remainingBalance);
    }

    private List<coin> collectChange() {
        long changeAmount = currentBalance - currentItem.getPrice();
        List<coin> change = getChange(changeAmount);
        updateCashInventory(change);
        currentBalance = 0;
        currentItem = null;
        return change;
    }

    public List<coin> refund(){
        List<coin> refund = getChange(currentBalance);
        updateCashInventory(refund);
        currentBalance = 0;
        currentItem = null;
        return refund;
    }


    private boolean isFullPaid() {
        if(currentBalance >= currentItem.getPrice()){
            return true;
        }
        return false;
    }


    private List<coin> getChange(long amount) throws NoChangeAvailableException{
        List<coin> changes = Collections.EMPTY_LIST;

        if(amount > 0){
            changes = new ArrayList<coin>();
            long balance = amount;
            while(balance > 0){
                if(balance >= coin.QUARTER.getValue()
                        && cashInventory.hasItem(coin.QUARTER)){
                    changes.add(coin.QUARTER);
                    balance = balance - coin.QUARTER.getValue();
                    continue;

                }else if(balance >= coin.DIME.getValue()
                        && cashInventory.hasItem(coin.DIME)) {
                    changes.add(coin.DIME);
                    balance = balance - coin.DIME.getValue();
                    continue;

                }else if(balance >= coin.NICKLE.getValue()
                        && cashInventory.hasItem(coin.NICKLE)) {
                    changes.add(coin.NICKLE);
                    balance = balance - coin.NICKLE.getValue();
                    continue;

                }else if(balance >= coin.PENNY.getValue()
                        && cashInventory.hasItem(coin.PENNY)) {
                    changes.add(coin.PENNY);
                    balance = balance - coin.PENNY.getValue();
                    continue;

                }else{
                    throw new NoChangeAvailableException("NotSufficientChange,Please try another product");
                }
            }
        }

        return changes;
    }

    public void reset(){
        cashInventory.clear();
        itemInventory.clear();
        totalSales = 0;
        currentItem = null;
        currentBalance = 0;
    }

    public void printStats(){
        System.out.println("Total Sales : " + totalSales);
        System.out.println("Current Item Inventory : " + itemInventory);
        System.out.println("Current Cash Inventory : " + cashInventory);
    }


    private boolean hasSufficientChange(){
        return hasSufficientChangeForAmount(currentBalance - currentItem.getPrice());
    }

    private boolean hasSufficientChangeForAmount(long amount){
        boolean hasChange = true;
        try{
            getChange(amount);
        }catch(NoChangeAvailableException nsce){
            return hasChange = false;
        }

        return hasChange;
    }

    private void updateCashInventory(List change) {
//        for(coin c : change){
//            cashInventory.sell(c);
//        }
    }

    public long getTotalSales(){
        return totalSales;
    }


}
